import time

import uiautomator2 as u2

answer_right_list = ["是", "帅", "帅哥", "对", "yes", "很帅", "宇宙第一帅"];

# USB连接
device = u2.connect();

# WiFi连接
# device = u2.connect_adb_wifi("192.168.1.9");

"""
    发送消息
"""


def auto_answer(message="我帅不帅"):
    # 点击对话框
    device(resourceId="com.tencent.mm:id/iki").click()
    # 切换输入法
    device.set_fastinput_ime(True)
    time.sleep(1)
    # 输入内容
    device.send_keys(message)
    # d.set_fastinput_ime(False)
    # 发送消息
    device(resourceId="com.tencent.mm:id/ay5").click()


"""
    打开微信
"""


def open_chat_window():
    # 根据包名启动微信
    device.app_start("com.tencent.mm")
    # 由于手机存在应用分身，所以多出一步
    device.xpath(
        '//*[@resource-id="vivo:id/resolver_slide"]/android.widget.LinearLayout[2]/android.widget.ImageView[1]').click()
    time.sleep(3)
    # d(resourceId="com.tencent.mm:id/dub", text="通讯录").click()
    # 打开置顶的聊天框
    device.xpath(
        '//*[@resource-id="com.tencent.mm:id/f67"]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]').click()


def is_right_answer(context):
    if context in answer_right_list:
        return True;
    return False;


"""
获取最新内容
"""


def get_newest_answer():
    # 统计所有的聊天框
    count = len(device.xpath('//*[@resource-id="com.tencent.mm:id/awv"]/android.widget.RelativeLayout').all());
    # 获取最底下的聊天信息
    ele = device.xpath('//*[@resource-id="com.tencent.mm:id/awv"]/android.widget.RelativeLayout[' + str(
        count) + ']/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]');

    x, y = ele.center();
    window_x, window_y = device.window_size();
    if x == window_x / 2:  # 相等则是表情包
        ele = device.xpath('//*[@resource-id="com.tencent.mm:id/awv"]/android.widget.RelativeLayout[' + str(
            count) + "]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]");
        if ele.exists:
            x, y = ele.center();
    return x, y;


"""
    获取恢复内容
"""


def get_answer_content():
    x, y = get_newest_answer();
    #这里由于会有偏差，所有再取28像素的中心点
    cent_x = (x + (x - 28)) / 2;
    cent_y = (y + (y - 28)) / 2;
    device.click(cent_x, cent_y);
    time.sleep(0.1)
    device.click(cent_x, cent_y);

    ele = device(resourceId="com.tencent.mm:id/dc3");
    text = "";
    if ele.exists:
        text = str(device(resourceId="com.tencent.mm:id/dc3").get_text()).strip();
        # 关闭放大框
        device(resourceId="com.tencent.mm:id/dc3").click();
    else:
        # 是不是表情包
        ele = device(resourceId="com.tencent.mm:id/ei");
        if ele.exists:
            device(resourceId="com.tencent.mm:id/ei").click();
            text = "这" \
                   "是一个表情包"
        else:
            # 图片
            device.click(x, y);
            text = "这是一张图片"

    return text;


"""
    判断是否是回复
"""


def is_answer():
    x, y = get_newest_answer();
    window_x, window_y = device.window_size();
    # 如果在屏幕的左侧则是回复
    if x < window_x / 2:
        return True;
    return False;


def start():
    open_chat_window();
    auto_answer();


if __name__ == '__main__':
    start();
    while True:
        if is_answer():
            text = get_answer_content();
            print(text)
            if is_right_answer(text):
                break
            else:
                auto_answer();
        #等待5秒在扫描
        time.sleep(5)
    auto_answer("这就对了")
    device.set_fastinput_ime(False)
